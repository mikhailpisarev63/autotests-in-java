package Page;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class PageWebDriver {

    public static WebDriver driver;
    PageWebDriver(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public static LoginPage loginPage;
    public static ProfilePage profilePage;

    public static String url = "https://mail.yandex.ru/";
    public static String login = "test63rus";
    public static String password = "test63test";

    /**
     * осуществление первоначальной настройки
     */

    @BeforeClass
    public static WebDriver setUp()
    {
        System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(url);

        loginPage = new LoginPage(driver);
        profilePage = new ProfilePage(driver);

        yandexAuthorization();

        return driver;
    }

    public static void yandexAuthorization(){
        /**
         * Авторизация на яндекс почте
         */

        /* Клик по кнопки "Войти на почту". Выбор регистрации по логину. Ввести логин или email.
           Клик по кнопке "Войти. Ввести пароль */
        loginPage.buttonMailElement().registrationByLogin().enterYourEmailAddress(login).loginButton().
                enteringPassword(password);

        //(loginPage) Клик по кнопке "Войти"
        loginPage.loginButton();
    }
}
