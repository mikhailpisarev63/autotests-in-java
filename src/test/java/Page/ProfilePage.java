package Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProfilePage extends LoginPage {

    /**
     * Локаторы с главной страницы Яндекс почта
     */

    public ProfilePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "a[href='#compose']")
    WebElement writeALetter; //написать письмо

    @FindBy(css = "div#compose-field-1")
    WebElement email; //поле email

    @FindBy(css = "div.cke_wysiwyg_div")
    WebElement text; //поле текста

    @FindBy(css = "div.new__root--3qgLa button.Button2_view_default")
    WebElement sendButton; //кнопка отправить

    @FindBy(xpath = "//div[@class='ComposeDoneScreen-Title']//span[text()='Письмо отправлено']")
    WebElement textTest;

    @FindBy(css = ".ns-view-toolbar-button-delete")
    WebElement deleteElement;  //Кнопка удалить

    @FindBy(css = "._nb-checkbox-flag")
    WebElement checkboxElement;

    public ProfilePage writeALetter(){
        writeALetter.click();
        return this;
    }

    public ProfilePage email(String mail){
        email.sendKeys(mail);
        return this;
    }

    public ProfilePage text(String textForTheField){
        text.sendKeys(textForTheField);
        return this;
    }

    public ProfilePage sendButton(){
        sendButton.click();
        return this;
    }

    public String getTextTest(){
        return textTest.getText();
    }

    public ProfilePage deleteElement(){
        deleteElement.click();
        return this;
    }
}
