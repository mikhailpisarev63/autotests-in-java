package Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageWebDriver {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Локаторы для входа и ввода данных на почту
     */

    @FindBy(css = "a.Button2_view_default")
    WebElement buttonMailElement; //Кликаем на Вход в почту

    @FindBy(css = "button[data-type='login']")
    WebElement registrationByLogin; //Выбор регистрации по логину

    @FindBy(css = "input#passp-field-login")
    WebElement enterYourEmailAddress; //ввод mail или login

    @FindBy(css = "button[id^='pass'].Button2")
    WebElement loginButton; //кнопка войти

    @FindBy(css = "input#passp-field-passwd.Textinput-Control")
    WebElement enteringPassword; //ввод пароля

    public LoginPage buttonMailElement(){
        buttonMailElement.click();
        return this;
    }

    public LoginPage registrationByLogin(){
        registrationByLogin.click();
        return this;
    }

    public LoginPage enterYourEmailAddress(String login){
        enterYourEmailAddress.sendKeys(login);
        return this;
    }

    public ProfilePage loginButton(){
        loginButton.click();
        return new ProfilePage(driver);
    }

    public LoginPage enteringPassword(String password){
        enteringPassword.sendKeys(password);
        return this;
    }
}
