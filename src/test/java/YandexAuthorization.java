import Page.PageWebDriver;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import static Page.PageWebDriver.*;

public class YandexAuthorization {

    private static WebDriver driver;

    @Before
    public void setDriver(){
        driver = PageWebDriver.setUp();
        PageFactory.initElements(driver, this);
    }

    Random random = new Random();

    public String testText = "Письмо отправлено";
    public String mail = "test63rus@yandex.ru";
    public String textForTheField = "Добрый день!\n" +
            "\n" +
            "Простейший автотест готов.\n" +
            "\n" +
            "С Уважением, \n" +

            "Писарев Михаил Олегович";

    @Test
    public void SendingMessage() throws IOException {

        /**
         * Отправим три раза сообщение используя цикл for, это нам позволит
         * заполнить пустую почту сообщениями и продолжить дальнейшую работу
         */

        for (int i = 0; i < 3; i++)
        {
            //(ProfilePage) Клик написать письмо. Ввод данные email. Ввод текста. Отправить письмо
            profilePage.writeALetter().email(mail).text(textForTheField).sendButton();

            //Проверка по уведомлению: Письмо отправлено
            Assert.assertEquals("Text", testText, profilePage.getTextTest());

            driver.navigate().refresh();
        }
        tearDown();
    }

    @Test
    public void CheckboxClick() throws IOException{

        List<WebElement> checkbox = driver.findElements(By.cssSelector("._nb-checkbox-flag"));
        //Запускаем цикл до 2 раз
        for (int a = 0; a < 2; a++)
        {
            //Случайный выбор 2-х чекбокс из длины списка
            int check = random.nextInt(checkbox.size());
            //проверяем выбран ли элемент на странице или нет
            if (!checkbox.get(check).isSelected()){
                checkbox.get(check).click();
            }
        }
        tearDown();
    }

    @Test
    public void DeleteMessage() {

        List<WebElement> checkbox = driver.findElements(By.cssSelector("._nb-checkbox-flag"));

        //Случайны выбор сообщения
        int check = random.nextInt(checkbox.size());
        //проверяем выбран ли элемент на странице или нет
        if (!checkbox.get(check).isSelected()) {
            checkbox.get(check).click();
            //Удаляем элемент
            profilePage.deleteElement();
        }
    }

    @AfterClass
    public static void tearDown() throws IOException {
        var sourceFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(sourceFile, new File("d:\\загрузки\\screenshot.png"));
        driver.quit();
    }
}
